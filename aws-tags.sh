#!/bin/sh
#
# This script is automate the process of finding tags of AWS resources.
# 1. ```saml2aws -a default login``` ( where default is referring to the profile name specified in the $HOME/.saml2aws file )
# 2. It will generate $HOME/.aws/credentials file that contains all temporary credentials of AWS account.
# 3. ```saml2aws -a default exec aws ecs list-clusters -- --region eu-west-1``` list all available clusters and its resource arn
# 4. ```saml2aws -a default exec aws ecs list-tags-for-resource -- --region eu-west-1 --resource-arn ${resource_arn}``` list all tags from selected cluster
#
# Known issues:
# 1. It show blank when listing all available clusters and its resource arn,
#    Delete $HOME/.aws/credentials and try again.
#
# Installation
# ```brew install saml2aws awscli fzf gawk jq```

REGION=eu-west-1
DEFAULT_CLUSTER=central-shared-
DEFAULT_OKTA_URL=https://williamhill-cocloudservices.okta.com/home/amazon_aws/0oaco29stYyRCcx5F355/272
DEFAULT_MFA=PUSH

check_installed() {
  # defined an array for all dependent commands
  set -- "saml2aws" "aws" "fzf" "gawk" "jq"
  # checks if commands are installed
  for command in "${@}"; do
    if ! type "$command" >/dev/null 2>/dev/null; then
      echo "Please install $command"
      exit 64 # EX_USAGE
    fi
  done
}

select_profile() {
  # if $HOME/.saml2aws file does not exists
  if [ ! -f "${HOME}/.saml2aws" ]; then
    nonprod_arn="arn:aws:iam::257254267474:role/williamhill-delegated-read"
    printf "%s\n%s\n%s" "Please create $HOME/.saml2aws file by running below command" \
      'saml2aws configure -a <profile_name> --role=<role_arn> -p <aws_profile>' \
      "e.g. saml2aws configure -a default --role=$nonprod_arn \
      -p default --mfa=$DEFAULT_MFA --url=$DEFAULT_OKTA_URL"

    exit 78 # EX_CONFIG
  fi
  selected_profile=$(sed -nr "s/\[(.*)\]/\1/p" "${HOME}/.saml2aws" |
    fzf --header="Select profile" --header-first --no-preview)
  if [ -z "$selected_profile" ]; then
    exit 0
  fi
}

login() {
  # check if [selected_profile] exists in $HOME/.aws/credentials
  if ! grep -q "\[$selected_profile\]" "$HOME/.aws/credentials"; then
    # echo "[$selected_profile] not found. Please delete your $HOME/.aws/credentials and run this command again.";
    saml2aws -a "${selected_profile}" login
  fi

  saml2aws script >/dev/null 2>&1
  retVal=$?
  if [ $retVal -ne 0 ]; then
    echo "Please login first"
    saml2aws -a "${selected_profile}" login
    retVal=$?
    if [ $retVal -ne 0 ]; then
      echo "Login failed"
      exit 0
    fi
  fi
}

select_cluster() {
  # selected_cluster=$(echo "$AVS_CLUSTERS" | fzf --header="Select cluster" --header-first --no-preview)
  selected_cluster=$(
    saml2aws -a "${selected_profile}" exec \
      aws ecs list-clusters -- --region ${REGION} |
      jq -r '.clusterArns[]' | sort |
      fzf --header-first --header="Select service for ${selected_profile}" -q "${DEFAULT_CLUSTER}" \
        --delimiter="/" --with-nth -1 --no-preview
  )
  if [ -z "$selected_cluster" ]; then
    exit 0
  fi
}

select_service_arn() {
  # https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_ListTagsForResource.html
  list_tags_command="saml2aws -a ${selected_profile} exec \
    aws ecs list-tags-for-resource -- --region ${REGION} --resource-arn {} |
    jq -r '.tags[] | \"\(.key)|\(.value)\"' | column -t -s \|"
  selected_service_arn=$(
    saml2aws -a "${selected_profile}" exec \
      aws ecs list-services -- --cluster "${selected_cluster}" --region ${REGION} |
      jq -r '.serviceArns[]' | sort |
      fzf --header-first --header="Select service for ${selected_cluster}" --delimiter="/" --with-nth -1 \
        --preview="${list_tags_command}"
  )

  if [ -z "$selected_service_arn" ]; then
    exit 0
  fi
}

select_service_name() {
  selected_service_name=$(echo "${selected_service_arn}" | awk -F "/" '{ print $NF }')
  echo "Service ARN: ${selected_service_arn}"

  list_tags_command=$(echo "${list_tags_command}" | sed "s#{}#${selected_service_arn}#g")
  eval "${list_tags_command}" | fzf --no-preview --header-first --header="${selected_service_name}"
}

check_installed
select_profile
login
select_cluster
select_service_arn
select_service_name
